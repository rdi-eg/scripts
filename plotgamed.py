import matplotlib.pyplot as plt
import sys

with open(sys.argv[1]) as f:
	lines = [float(i) for i in f.read().splitlines()]

plt.plot(lines)
plt.ylabel('some numbers')
plt.show()
